@scenario=postcode
Feature: Post code tests
  
   Scenario Outline: User makes a get request for postcode  
    
    When making a get request with <postCode> I should get a <responseCode> response code
    
    Examples: 
		|postCode        | responseCode |     
		|SE12SB          | 200          |
		|INVALIDPOSTCODE | 404          |
		|                | 400          |