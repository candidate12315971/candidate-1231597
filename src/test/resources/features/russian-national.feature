@scenario=russia
Feature: Russian national planning to visit UK checks Visa requirements
  
  Background: User accesses the UK Gov site 
	Given I want to visit the UK
	And I am a Russia National
	When The purpose of my visit is Tourism

   Scenario: Russian national not traveling with partner   
            
    And I am traveling without my partner
    Then I do need a Visa  
    
   
  Scenario Outline: Russian national traveling with partner. 
            
    And I am traveling with my partner
    And my partner <articleCard> article card 
    Then I <VisaRequirement> need a Visa
	
	Examples: 
		|articleCard | VisaRequirement |     
		|has         |do not           |
		|doesNotHave |do               |