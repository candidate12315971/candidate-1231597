@scenario=error
Feature: Visa check user journey negative tests 
  
  Background: User accesses the UK Gov site 
	Given I want to visit the UK
	
	

   Scenario: User does not select purpose of visit  
    
    And I am a Japan National        
    And I do not select the purpose of my visit
    Then I should see an error
    
   Scenario: User does not select duration of stay
   
    And I am a Japan National
    And The purpose of my visit is Study
    When I do not select the duration of stay
    Then I should see an error on Duration of Stay Page
    
    
    Scenario: User does not select response for partner question
   
    And I am a Russia National
    And The purpose of my visit is Tourism
    When I do not select the partner question
    Then I should see an error on Traveling with partner question page
    
   
    Scenario: User does not select response for article card question
   
    And I am a Russia National
    And The purpose of my visit is Tourism
    And I am traveling with my partner
    When I do not select the article card question
    Then I should see an error on article card question page
    