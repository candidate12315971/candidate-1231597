package com.assessment.cucumber.steps.serenity;

//Serenity Steps from POM Pages


import com.assessment.pages.CheckUkVisaPage;
import com.assessment.pages.SelectCountryPage;
import com.assessment.pages.CountryQuestionPage;
import com.assessment.pages.DurationOfStayPage;
import com.assessment.pages.TravelingWithPartnerQuestionPage;
import com.assessment.pages.ArticleQuestionPage;
import com.assessment.pages.VisaResultPage;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class AssessmentSerenitySteps extends ScenarioSteps {

	CheckUkVisaPage checkUkVisaPage;
	SelectCountryPage selectCountryPage;
	CountryQuestionPage countryQuestionPage;
	VisaResultPage visaResultPage;
	DurationOfStayPage durationOfStayPage;
	TravelingWithPartnerQuestionPage travelingWithPartnerQuestionPage;
	ArticleQuestionPage articleQuestionPage;

	@Step("Click to get started for checking Visa requirement")
	public void clickStartNow() {

		checkUkVisaPage.clickStartNow();

	}

	@Step("Select the Country of Nationality and navigate to next Page")

	public void selectCountry(String countryName) {

		selectCountryPage.selectCountry(countryName);

	}

	@Step("Select the purpose of visit and navigate to next Page")

	public void selectPurposeOfVist(String purposeOfVisit, String countryName) {

		countryQuestionPage.selectPurposeOfVist(purposeOfVisit, countryName);

	}

	@Step("Select the duration of Stay for Study")

	public void selectDurationofVisit(String durationOfStay) {

		durationOfStayPage.selectDurationOfStay(durationOfStay);

	}

	@Step("Is Traveling With partner")

	public void travellingWithPartner() {

		travelingWithPartnerQuestionPage.travellingWithPartner();

	}

	@Step("Is Not Traveling With partner")

	public void travellingWithoutPartner() {

		travelingWithPartnerQuestionPage.travellingWithoutPartner();

	}

	@Step("Has an article 10 or 20 card")

	public void hasArticleCard() {

		articleQuestionPage.hasArticleCard();

	}

	@Step("Does not have an article 10 or 20 card")

	public void doesNotHaveArticleCard() {

		articleQuestionPage.doesNotHaveArticleCard();

	}

	@Step("Get a decision on whether or not the visitor will need a Visa")

	public boolean isVisaRequired() {

		return visaResultPage.isVisaRequired();

	}

	
	@Step("User does not select Purpose of Visit")

	public void userDoesNotSelectAPurpose() {

		countryQuestionPage.userDoesNotSelectAPurpose();

	}

	
	@Step("user do not select a purpose of visit and try to move ahead in the process")

	public boolean userSeesAnError() {

		return countryQuestionPage.userSeesAnError();

	}
	
	
	
	@Step("user does not select duration of stay")

	public void userDoesNotSelectADuration() {

		durationOfStayPage.userDoesNotSelectADuration();

	}
	
	@Step("User sees an error on Duration of stay page")

	public boolean userSeesAnErrorOnDurationOfStayPage() {

		return durationOfStayPage.userSeesAnErrorOnDurationOfStayPage();

	}
	
	
	@Step("user does not select Partner question")

	public void userDoesNotSelectPartnerQuestion() {

		travelingWithPartnerQuestionPage.userDoesNotSelectPartnerQuestion();

	}
	
	@Step("User sees an error on travelling with partner page page")

	public boolean userSeesAnErrorOnTravelingWithPartnerPage() {

		return travelingWithPartnerQuestionPage.userSeesAnErrorOnTravelingWithPartnerPage();

	}
	
	@Step("user does not select artilce card question")

	public void userDoesNotSelectArticleCardQuestion() {

		articleQuestionPage.userDoesNotSelectArticleCardQuestion();

	}
	
	@Step("User sees an error on article card page")

	public boolean userSeesAnErrorOnArticleCardPage() {

		return articleQuestionPage.userSeesAnErrorOnArticleCardPage();

	}

}
