package com.assessment.cucumber.steps;

//Cucumber Steps mapped to Scenario steps

import static org.junit.Assert.assertEquals;

import java.util.List;

import com.assessment.cucumber.steps.serenity.AssessmentSerenitySteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Steps;

public class AssessmentCucumberSteps {

	@Steps
	AssessmentSerenitySteps visaSiteVisitor;

	@Given("^I want to visit the UK$")
	public void i_want_to_visit_the_UK() {
		visaSiteVisitor.clickStartNow();
	}

	@Given("^I am a (.*) National$")
	public void i_am_a_NonUK_National(String countryName) {

		Serenity.setSessionVariable("countryName").to(countryName);
		visaSiteVisitor.selectCountry(countryName);

	}

	@When("^The purpose of my visit is Tourism$")
	public void the_purpose_of_my_visit_is_Tourism() {
		String countryName = Serenity.sessionVariableCalled("countryName");
		visaSiteVisitor.selectPurposeOfVist("Tourism", countryName);
	}

	@Then("^I (.*) need a Visa$")
	public void do_i_need_a_Visa(String visaRequired) {

		boolean expectedResult = false;

		if (visaRequired.contentEquals("do")) {

			expectedResult = true;
		}

		boolean isVisaRequired = visaSiteVisitor.isVisaRequired();
		assertEquals(expectedResult, isVisaRequired);
	}

	@When("^The purpose of my visit is Study$")
	public void the_purpose_of_my_visit_is_Study() {
		String countryName = Serenity.sessionVariableCalled("countryName");
		visaSiteVisitor.selectPurposeOfVist("Study", countryName);
	}

	@When("^duration of my stay is (.*)$")
	public void duration_of_my_stay(String durationOfStay) {
		visaSiteVisitor.selectDurationofVisit(durationOfStay);
	}

	@When("^I am traveling without my partner$")
	public void travellingWithoutPartner() {
		visaSiteVisitor.travellingWithoutPartner();
	}

	@When("^I am traveling with my partner$")
	public void travellingWithPartner() {
		visaSiteVisitor.travellingWithPartner();
	}

	@When("^my partner (.*) article card$")
	public void articleCardCheck(String articleCard) {

		if (articleCard.contentEquals("has")) {
			visaSiteVisitor.hasArticleCard();
		} else {

			visaSiteVisitor.doesNotHaveArticleCard();
		}
	}
	
	@When("^I do not select the purpose of my visit$")
	public void userDoesNotSelectAPurpose() {
		visaSiteVisitor.userDoesNotSelectAPurpose();
	}
	
	@Then("^I should see an error$")
	public void userSeesAnError() {
		assertEquals(true, visaSiteVisitor.userSeesAnError());
	}

	
	@When("^I do not select the duration of stay$")
	public void userDoesNotSelectADuration() {
		visaSiteVisitor.userDoesNotSelectADuration();
	}
	
	@Then("^I should see an error on Duration of Stay Page$")
	public void userSeesAnErrorOnDurationOfStayPage() {
		assertEquals(true, visaSiteVisitor.userSeesAnErrorOnDurationOfStayPage());
	}
	
	
	@When("^I do not select the partner question$")
	public void userDoesNotSelectPartnerQuestion() {
		visaSiteVisitor.userDoesNotSelectPartnerQuestion();
	}
	
	@Then("^I should see an error on Traveling with partner question page$")
	public void userSeesAnErrorOnTravelingWithPartnerPage() {
		assertEquals(true, visaSiteVisitor.userSeesAnErrorOnTravelingWithPartnerPage());
	}

	
	@When("^I do not select the article card question$")
	public void userDoesNotSelectArticleCardQuestion() {
		visaSiteVisitor.userDoesNotSelectArticleCardQuestion();
	}
	
	@Then("^I should see an error on article card question page$")
	public void userSeesAnErrorOnArticleCardPage() {
		assertEquals(true, visaSiteVisitor.userSeesAnErrorOnArticleCardPage());
	}

	@When("^making a get request with (.*) I should get a (.*) response code$")
	public void when_making_a_get_request_the_response_is_dependant_on_my_path_parameter(String postCode, int responseCode) {
		
	String pathParameter = "/" + 	postCode;
	     RestAssured.given()
	    .when()
	    .get(pathParameter)
	    .then()
	    .statusCode(responseCode);
	    
	}	

	
}
