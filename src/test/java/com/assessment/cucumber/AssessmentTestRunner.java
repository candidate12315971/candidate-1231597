package com.assessment.cucumber;

//Provides Runner for Cucumber in Serenity.


import org.junit.runner.RunWith;
import com.assessment.setapibaseurl.SetApiBaseUrl;
import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/", tags= "@scenario=japan,@scenario=russia,@scenario=error,@scenario=postcode" )
public class AssessmentTestRunner extends SetApiBaseUrl{

}
