package com.assessment.pages;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.WebElementFacade;

//country selection page

public class SelectCountryPage extends CheckUkVisaPage {
	
	private static final String COUNTRY_SELECTOR = "//select[@id='response']";
	private static final String NEXT_STEP = "//div[@id='current-question']/button";	
	public CountryQuestionPage selectCountry(String countryName) {	
		
		
	$(COUNTRY_SELECTOR).selectByVisibleText(countryName);
	
	
	$(NEXT_STEP).click();
	
	return this.switchToPage(CountryQuestionPage.class);
	
	}
	
}
