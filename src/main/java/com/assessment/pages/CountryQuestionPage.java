package com.assessment.pages;

//Purpose of Visit page. Selection following the page where country is selected

public class CountryQuestionPage extends SelectCountryPage {

	private static final String TOURISM = "//input[@value='tourism']";
	private static final String WORK = "//input[@value='work']";
	private static final String STUDY = "//input[@value='study']";
	private static final String TRANSIT = "//input[@value='transit']";
	private static final String FAMILY = "//input[@value='family']";
	private static final String MARRIAGE = "//input[@value='marriage']";
	private static final String SCHOOL = "//input[@value='school']";
	private static final String MEDICAL = "//input[@value='medical']";
	private static final String DIPLOMATIC = "//input[@value='diplomatic']";
	private static final String ERROR_MESSAGE = "//div[@id='current-question']/div/div/fieldset/span";
	private static final String NEXT_STEP = "//button[contains(.,'Next step')]";

	public ResultSuperPage selectPurposeOfVist(String purposeOfVisit, String countryName) {
		String selection = "";

		switch (purposeOfVisit) {
		case "Tourism":
			selection = TOURISM;
			break;
		case "Study":
			selection = STUDY;
		default:
			// code block
		}

		if ((countryName == "Japan") && selection.contentEquals(TOURISM)) {
			$(selection).click();
			waitFor(NEXT_STEP).$(NEXT_STEP).click();			
			return this.switchToPage(VisaResultPage.class);

		} else if (countryName == "Russia") {

			$(selection).click();
			waitFor(NEXT_STEP).$(NEXT_STEP).click();
			return this.switchToPage(TravelingWithPartnerQuestionPage.class);

		} else {

			$(selection).click();
			waitFor(NEXT_STEP).$(NEXT_STEP).click();
			return this.switchToPage(DurationOfStayPage.class);
		}

	}
	
	public void userDoesNotSelectAPurpose() {
		
		waitFor(NEXT_STEP).$(NEXT_STEP).click();
		
		
	}
	
	public boolean userSeesAnError()
	
	{
		boolean isError = $(ERROR_MESSAGE).isVisible();		
		return isError;
		
	}

}
