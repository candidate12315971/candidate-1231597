package com.assessment.pages;

//Duration of Stay Page

public class DurationOfStayPage extends ResultSuperPage {

	private static final String SIX_MONTHS_OR_LESS = "//input[@value='six_months_or_less']";
	private static final String LONGER_THAN_SIX_MONTHS = "//input[@value='longer_than_six_months']";
	private static final String ERROR_MESSAGE = "//div[@id='current-question']/div/div/fieldset/span";
	private static final String NEXT_STEP = "//button[contains(.,'Next step')]";

	public VisaResultPage selectDurationOfStay(String durationOfStay) {
		
		
		 
		 if(durationOfStay.contentEquals("SixMonthsOrLess")){
			 
			 $(SIX_MONTHS_OR_LESS).click();
		 
		 }
		 else {
			 
		 $(LONGER_THAN_SIX_MONTHS).click();
		 }
		 
		//$(SIX_MONTHS_OR_LESS).click(); 
		waitFor(NEXT_STEP).$(NEXT_STEP).click();
		return this.switchToPage(VisaResultPage.class);
	 }
	
		public void userDoesNotSelectADuration() {

			waitFor(NEXT_STEP).$(NEXT_STEP).click();

		}

		public boolean userSeesAnErrorOnDurationOfStayPage()

		{
			boolean isError = $(ERROR_MESSAGE).isVisible();
			return isError;

		}

}
