package com.assessment.pages;

// Page for Traveling with Partner questions
public class TravelingWithPartnerQuestionPage extends ResultSuperPage {

	private static final String TRAVELLING_WITH_PARTNER = "//input[@value='yes']";
	private static final String TRAVELLING_WITHOUT_PARTNER = "//input[@value='no']";
	private static final String ERROR_MESSAGE = "//div[@id='current-question']/div/div/fieldset/span";
	private static final String NEXT_STEP = "//button[contains(.,'Next step')]";

	public VisaResultPage travellingWithoutPartner() {

		$(TRAVELLING_WITHOUT_PARTNER).click();
		waitFor(NEXT_STEP).$(NEXT_STEP).click();

		return this.switchToPage(VisaResultPage.class);
	}

	public ArticleQuestionPage travellingWithPartner() {

		$(TRAVELLING_WITH_PARTNER).click();
		waitFor(NEXT_STEP).$(NEXT_STEP).click();

		return this.switchToPage(ArticleQuestionPage.class);
	}

	public void userDoesNotSelectPartnerQuestion() {

		waitFor(NEXT_STEP).$(NEXT_STEP).click();

	}

	public boolean userSeesAnErrorOnTravelingWithPartnerPage()

	{
		boolean isError = $(ERROR_MESSAGE).isVisible();
		return isError;

	}

}
