package com.assessment.pages;

public class ArticleQuestionPage extends TravelingWithPartnerQuestionPage {
	
	private static final String HAS_ARTICLE_CARD = "//input[@value='yes']";
	private static final String DOES_NOT_HAVE_ARTICLE_CARD = "//input[@value='no']";
	private static final String ERROR_MESSAGE = "//div[@id='current-question']/div/div/fieldset/span";
	private static final String NEXT_STEP = "//button[contains(.,'Next step')]";
	
public VisaResultPage hasArticleCard() {
		
		$(HAS_ARTICLE_CARD).click();
		waitFor(NEXT_STEP).$(NEXT_STEP).click();
		
		return this.switchToPage(VisaResultPage.class);
	}
	
	public VisaResultPage doesNotHaveArticleCard() {

		$(DOES_NOT_HAVE_ARTICLE_CARD).click();
		waitFor(NEXT_STEP).$(NEXT_STEP).click();

		return this.switchToPage(VisaResultPage.class);
	}


	public void userDoesNotSelectArticleCardQuestion() {

		waitFor(NEXT_STEP).$(NEXT_STEP).click();

	}

	public boolean userSeesAnErrorOnArticleCardPage()

	{
		boolean isError = $(ERROR_MESSAGE).isVisible();
		return isError;

	}


}
