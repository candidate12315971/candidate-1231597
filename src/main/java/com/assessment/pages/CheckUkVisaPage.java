package com.assessment.pages;

import net.serenitybdd.core.pages.PageObject;

//Landing Page for the user journey start

public class CheckUkVisaPage extends  PageObject{
	
	private static final String ACCEPT_COOKIES = "//*[@data-accept-cookies='true']";
	private static final String START_NOW = "//p[@id='get-started']/a";

    public SelectCountryPage clickStartNow() {
		
    	open();
        getDriver().manage().window().maximize();    	
    	
		waitFor(ACCEPT_COOKIES).$(ACCEPT_COOKIES).click();
    	
		waitFor(START_NOW).$(START_NOW).click();
		
		return this.switchToPage(SelectCountryPage.class);
	}


}
