package com.assessment.pages;

// Result Page. Decide whether or not visa required

public class VisaResultPage extends ResultSuperPage {
	
	private static final String VISA_REQUIRED_RESPONSE = "//div[@id='result-info']/div[2]";
	
	public boolean isVisaRequired() {
		
		String visaRequiredResponse = waitFor(VISA_REQUIRED_RESPONSE).$(VISA_REQUIRED_RESPONSE).getTextValue();		
		
		if(visaRequiredResponse.contains("You won’t need a visa to come to the UK") || visaRequiredResponse.contains("You do not need a visa to come to the UK")) {			
			
			return false;
		}
		else {			
			
			return true;
		}
	}

}
