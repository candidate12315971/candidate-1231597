# Assessment Project
The Project achieves automation testing for the following use cases.

1. Japanese National on UK gov site checking to see if they need a visa to come to UK for Tourism or Study.
2. Russian National on UK gov site checking to see if they need a visa to come to UK for Tourism.
3. A user making a GET request to the api at [http://api.postcodes.io/postcodes](api.postcodes.io/postcodes)


## Requisites
The Project will need the following pre-installed software to run

1. Java9
2. Maven

## Web Drivers and browsers

1. Tests can be run on Chrome and FireFox
2. WebDrivers are included as part of the Project. These may need to be updated depending on the OS the browsers are hosted on
3. Path to WebDriver resources.
   - src/main/test/java/resources/drivers
	 - /linux
	 - /mac
	 - /windows
	 
## Project Structure
 
![Floder Structure](https://gitlab.com/candidate12315971/candidate-1231597/uploads/5d196a5d4e63c5674b18cd791807bc7c/Folder_Structure.jpg)

## Executing The Test Cases

1. Use "clean verify serenity:aggregate". 
2. Alternatively test can be run as Junit Tests.
3. When running via option 1. Aggregated Test results will be generated at /target/site/serenity/index.html
4. The Feature files are tagged. The current configuration of the AssessmentTestRunner Class has  
tags= "@scenario=japan,@scenario=russia,@scenario=error,@scenario=postcode". If only a subset feature needs to be run, this can be managed via the tags

## Feature File Description
1. **japanese-national.feature**.This will generate 3 Cucumber Scenarios. (Note, this appears as 2 scenarios in the Serenity Report with the second one having 2 examples. This applies to scenarios generated via Scenario Outline)
2. **russian-national.feature** .This will generate 3 Cucumber Scenarios.
3. **visa-negative-scenarios**. This will generate 4 Cucumber Scenarios.
4. **postcode.feature**. This will generate 3 Cucumber Scenarios.

## A Sample OutPut Report has been provided with the code.
